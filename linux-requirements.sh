#!/bin/bash
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:george-edison55/cmake-3.x
sudo apt-get update

sudo apt-get install build-essential libevent-dev libx11-dev cmake git
