include(FindPackageHandleStandardArgs)

if (MSVC)
    set(WebRTC_LIBRARY_DIRS ${CMAKE_BINARY_DIR}/lib ${CMAKE_BINARY_DIR}/lib/${CMAKE_BUILD_TYPE})
else()
    set(WebRTC_LIBRARY_DIRS ${CMAKE_BINARY_DIR}/bin)
endif()

#message("search = ${WebRTC_LIBRARY_DIRS}")
# if(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
#     set(WEBRTC_LIBRARY_NAME webrtcd.lib)
# else()
#     set(WEBRTC_LIBRARY_NAME webrtc.lib)
# endif()

set(WebRTC_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/webrtc-src/src")

set(WebRTC_LIBRARIES webrtc)

find_package_handle_standard_args(WebRTC DEFAULT_MSG WebRTC_INCLUDE_DIRS WebRTC_LIBRARY_DIRS WebRTC_LIBRARIES)
