if (APPLE OR UNIX)
    if ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        set(GLIBCXX_DEBUG -D_GLIBCXX_DEBUG=1)
    endif()
    hunter_config(Boost 
        VERSION 1.63.0
        CMAKE_ARGS CMAKE_CXX_FLAGS=-fvisibility=hidden\ -fPIC\ ${GLIBCXX_DEBUG})
endif()
