include(FindPackageHandleStandardArgs)

if(NOT JSDoc_EXECUTABLE)
    # Look for an executable called "jsdoc".
    find_program(JSDoc_EXECUTABLE jsdoc)
endif()

find_package_handle_standard_args(JSDoc 
    FOUND_VAR JSDoc_FOUND 
    REQUIRED_VARS JSDoc_EXECUTABLE)