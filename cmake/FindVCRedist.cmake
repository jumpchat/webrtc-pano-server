set(PROGRAMFILES "ProgramFiles(x86)")
if (MSVC12)
    set(VCREDIST_DIR "$ENV{${PROGRAMFILES}}\\Microsoft Visual Studio 12.0\\VC\\redist\\1033")
    if (${CMAKE_CL_64})
        set(VCREDISTFILENAME vcredist_x64.exe)
    else(${CMAKE_CL_64})
        SET(VCREDISTFILENAME vcredist_x86.exe)
    endif(${CMAKE_CL_64})
elseif (MSVC14)
    set(VCREDIST_DIR "$ENV{${PROGRAMFILES}}/Microsoft Visual Studio 14.0/VC/redist/1033")
    if (${CMAKE_CL_64})
        set(VCREDISTFILENAME vcredist_x64.exe)
    else(${CMAKE_CL_64})
        SET(VCREDISTFILENAME vcredist_x86.exe)
    endif(${CMAKE_CL_64})
endif()

find_file(
	VCREDIST
    NAMES ${VCREDISTFILENAME}
	PATHS "${VCREDIST_DIR}"
)

IF( VCREDIST )
      MESSAGE( STATUS "Found VCREDIST: ${VCREDIST}" )
      SET( VCREDIST_FOUND YES )
ELSE (VCREDIST)
   SET( VCREDIST_FOUND NO )
   IF (VCRedist_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find VCREDIST")
   ENDIF (VCRedist_FIND_REQUIRED)
ENDIF (VCREDIST)