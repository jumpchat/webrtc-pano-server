find_package (Doxygen)
IF (DOXYGEN_FOUND)
    IF (EXISTS ${PROJECT_SOURCE_DIR}/Doxyfile)
        add_custom_target(
            doxygen
            COMMAND ${DOXYGEN_EXECUTABLE} Doxyfile
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
            COMMENT "Generating doxygen project documentation." VERBATIM
        )
    install(DIRECTORY ${PROJECT_SOURCE_DIR}/docs/html/
        DESTINATION docs/html
        COMPONENT Documentation)
    else (EXISTS ${PROJECT_SOURCE_DIR}/Doxyfile)
        add_custom_target(doxygen COMMENT "Doxyfile not found. Please generate a doxygen configuration file to use this target." VERBATIM)
    endif (EXISTS ${PROJECT_SOURCE_DIR}/Doxyfile)
else (DOXYGEN_FOUND)
    add_custom_target(doxygen COMMENT "Doxygen not found. Please install doxygen to use this target." VERBATIM)
endif (DOXYGEN_FOUND)


find_package(JSDoc)
if (JSDoc_FOUND)
    add_custom_target(jsdocs
        COMMAND ${JSDoc_EXECUTABLE} --verbose --destination docs/js ${PROJECT_SOURCE_DIR}/static/js/
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    )

    install(DIRECTORY ${PROJECT_BINARY_DIR}/docs/js/
        DESTINATION docs/js
        COMPONENT Documentation)
else()
    add_custom_target(jsdocs COMMENT "Doxygen not found. Please install doxygen to use this target." VERBATIM)
endif()

add_custom_target(docs)
add_dependencies(docs jsdocs doxygen)

