include(FindPackageHandleStandardArgs)

find_path(Crow_INCLUDE_DIRS 
    NAMES crow.h 
    PATHS "${MAINFOLDER}/third-party/crow/include" NO_DEFAULT_PATH)
find_package_handle_standard_args(Crow DEFAULT_MSG Crow_INCLUDE_DIRS)
                                  
