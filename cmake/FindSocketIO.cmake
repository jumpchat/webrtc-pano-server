include(FindPackageHandleStandardArgs)

find_path(SocketIO_INCLUDE_DIRS 
    NAMES sio_client.h
    PATHS "${MAINFOLDER}/third-party/socket.io-client-cpp/src" NO_DEFAULT_PATH)

set(SocketIO_INCLUDE_DIRS "${MAINFOLDER}/third-party/socket.io-client-cpp/src")
set(SocketIO_LIBRARIES sioclient)

find_package_handle_standard_args(SocketIO DEFAULT_MSG SocketIO_INCLUDE_DIRS SocketIO_INCLUDE_DIRS SocketIO_LIBRARIES)
