include(FindPackageHandleStandardArgs)

set(cppnetlib_INCLUDE_DIRS 
    ${MAINFOLDER}/third-party/cpp-netlib
    ${MAINFOLDER}/third-party/cpp-netlib/deps/uri/include
    )
set(cppnetlib_LIBRARIES cppnetlib-client-connections cppnetlib-server-parsers cppnetlib-uri)

find_package_handle_standard_args(cppnetlib DEFAULT_MSG cppnetlib_INCLUDE_DIRS  cppnetlib_LIBRARIES)
