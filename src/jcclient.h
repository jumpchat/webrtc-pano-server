/****************************************************************************
 * Copyright (C) 2016, JumpChat                                             *
 * All rights reserved                                                      *
 *                                                                          *
 * For the licensing terms see LICENSE file inside the root directory.      *
 * For the list of contributors see AUTHORS file inside the root directory. *
 ****************************************************************************/
#pragma once

#include "jcclientinterface.h"
//#include "sio_client.h"

#include "socket_io_client.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/network/protocol/http/client.hpp>

class JCClient : public JCClientInterface {
public:
    JCClient();
    virtual ~JCClient();

    virtual void Connect(const std::map<std::string, std::string> &options);
    virtual void Disconnect();

    virtual bool JoinRoom(const std::string &url);

    // handlers
    // void OnConnected();
    // void OnClose(sio::client::close_reason const &reason);
    // void OnFail();

    // for testing
    std::string _testGetServerURL() { return _settings["serverUrl"].GetString(); }
    // std::string _testGetServerURL() { return ""; }

private:
    //sio::client *_client;
    boost::asio::io_service io_service;
    socketio::socketio_client_handler *_client;
    boost::network::http::client _httpclient;
    Document _settings;

    std::string _url;
    std::string _apiKey;
    std::string _userid;
    int _audioBandwidth;
    int _videoBandwidth;

    void ParseOptions(const std::map<std::string, std::string> &options);
};