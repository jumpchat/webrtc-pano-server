/****************************************************************************
 * Copyright (C) 2016, JumpChat                                             *
 * All rights reserved                                                      *
 *                                                                          *
 * For the licensing terms see LICENSE file inside the root directory.      *
 * For the list of contributors see AUTHORS file inside the root directory. *
 ****************************************************************************/
#pragma once

#ifdef WIN32
#define DLL_EXPORT __declspec(dllexport) 
#else
#define DLL_EXPORT  __attribute__((visibility("default")))
#endif


#include "jcclientinterface.h"