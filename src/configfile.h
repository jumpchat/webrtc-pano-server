/****************************************************************************
 * Copyright (C) 2016, FXPAL/JumpChat                                       *
 * All rights reserved                                                      *
 *                                                                          *
 * For the licensing terms see LICENSE file inside the root directory.      *
 * For the list of contributors see AUTHORS file inside the root directory. *
 ****************************************************************************/
#pragma once

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "webrtc/base/logging.h"
#include "crow.h"

class ConfigFile {

public:
    ConfigFile();
    ~ConfigFile();

    void Load(const std::string &filename);
    void Save(const std::string &filename);

    void Set(const std::string &section, const std::string &key, const std::string &value);

    int GetInt(const std::string &section, const std::string &key, int defaultValue);
	bool GetBool(const std::string &section, const std::string &key, bool defaultValue);
    std::string GetString(const std::string &section, const std::string &key, const std::string &defaultValue);

	crow::LogLevel GetServerLogLevel();
	rtc::LoggingSeverity GetWebRTCLogLevel();

private:
    boost::property_tree::ptree _config;

};