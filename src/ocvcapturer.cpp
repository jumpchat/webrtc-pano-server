#include "ocvcapturer.h"

#include "crow.h"
#include "webrtc/video_frame.h"
#include "webrtc/base/bind.h"
#include "webrtc/base/logging.h"
#include "webrtc/system_wrappers/include/critical_section_wrapper.h"
#include "webrtc/modules/video_capture/video_capture_factory.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

OCVCapturer::OCVCapturer() :
    _capturing(false),
    _crit(webrtc::CriticalSectionWrapper::CreateCriticalSection())
{
    _module = webrtc::VideoCaptureFactory::Create(_capture_input_interface);

    std::vector<cricket::VideoFormat> formats;
    formats.push_back(cricket::VideoFormat(1280, 720,
        cricket::VideoFormat::FpsToInterval(30), cricket::FOURCC_ARGB));  
}

OCVCapturer::~OCVCapturer() {
    Stop();
    delete _crit;
    //delete _module;
}

bool OCVCapturer::GetBestCaptureFormat(const cricket::VideoFormat& desired, cricket::VideoFormat* best_format) {
    return true;
}

cricket::CaptureState OCVCapturer::Start(const cricket::VideoFormat& capture_format) {
    _capturing = true;
    _module->RegisterCaptureDataCallback(this);

    CROW_LOG_INFO << "OCVCapturer started";
    SetCaptureState(cricket::CS_RUNNING);

    return cricket::CS_STARTING;
}

void OCVCapturer::Stop() {
    _capturing = false;
    _module->DeRegisterCaptureDataCallback();
    
    SetCaptureState(cricket::CS_RUNNING);
    SetCaptureFormat(NULL);
    CROW_LOG_INFO << "OCVCapturer stopped";
}

bool OCVCapturer::IsRunning() {
    //return _module != NULL && _module->CaptureStarted();
    return _capturing;
}

bool OCVCapturer::GetPreferredFourccs(std::vector<uint32_t>* fourccs) {
    fourccs->push_back(cricket::FOURCC_I420);
    return true;
}

void OCVCapturer::OnFrame(const webrtc::VideoFrame &frame) {

}

void OCVCapturer::IncomingFrame(const uint8_t *y, int y_size,
        const uint8_t *u, int u_size,
        const uint8_t *v, int v_size,        
        int width, int height, int rotation) {
    if (!IsRunning()) {
        return;
    }

    webrtc::VideoRotation rot = webrtc::kVideoRotation_0;

    switch (rotation) {
        case 0:
            rot = webrtc::kVideoRotation_0;
            break;
        case 90:
            rot = webrtc::kVideoRotation_90;
            break;
        case 180:
            rot = webrtc::kVideoRotation_180;
            break;
        case 270:
            rot = webrtc::kVideoRotation_270;
            break;
    }

    rtc::scoped_refptr<webrtc::I420Buffer> buffer = _bufferPool.CreateBuffer(width, height);
    memcpy(buffer->MutableDataY(), y, y_size);
    memcpy(buffer->MutableDataU(), u, u_size);
    memcpy(buffer->MutableDataV(), v, v_size);

    int64_t now = rtc::TimeNanos();
    int64_t render_time_us = now / rtc::kNumNanosecsPerMicrosec;
    int64_t ntp_time = now / rtc::kNumNanosecsPerMillisec;

    webrtc::VideoFrame frame(buffer, rot, render_time_us);
    frame.set_ntp_time_ms(ntp_time);
    VideoCapturer::OnFrame(frame, 
        buffer->width(), buffer->height());
}

