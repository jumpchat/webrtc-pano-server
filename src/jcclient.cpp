#include "jcclient.h"
#include "crow.h"

//#include "sio_client.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>

using namespace boost::network;

JCClient::JCClient() :
    _url("https://jump.chat/v1"),
    _audioBandwidth(0),
    _videoBandwidth(0)
{
    _userid = boost::lexical_cast<std::string>(boost::uuids::random_generator()());
    _client = new socketio::socketio_client_handler();

    // _client = new sio::client();
    // _client->set_open_listener(std::bind(&JCClient::OnConnected, this));
    // _client->set_close_listener(std::bind(&JCClient::OnClose, this, std::placeholders::_1));
    // _client->set_fail_listener(std::bind(&JCClient::OnFail, this));
}

JCClient::~JCClient() {
    // _client->sync_close();
    // _client->clear_con_listeners();
    delete _client;
}

void JCClient::Connect(const std::map<std::string, std::string> &options) {
    ParseOptions(options);        
    // _client->connect(_url);
}

void JCClient::Disconnect() {

}

bool JCClient::JoinRoom(const std::string &url) {
    std::string settings_url = url + "/settings";

    http::client::request request(settings_url);
    request << header("Connection", "close");
    
    http::client::response response = _httpclient.get(request);
    std::string response_body = body(response);

    if (status(response) == 200) {
        _settings.Parse(response_body.c_str());
        std::cout << "websocket url = " << _settings["serverUrl"].GetString() << std::endl;
        return true;
    }

    return false;
}

void JCClient::ParseOptions(const std::map<std::string, std::string> &options) {
    std::map<std::string, std::string>::const_iterator iter = options.find("serverName");
    if (iter != options.end()) {
        _url = (*iter).second;
    }

    iter = options.find("audioBandwidth");
    if (iter != options.end()) {
        _audioBandwidth = std::stoi((*iter).second);
    }
    
    iter = options.find("videoBandwidth");
    if (iter != options.end()) {
        _videoBandwidth = std::stoi((*iter).second);
    }
}

// Create/Destroy
extern "C" DLL_EXPORT JCClientInterface *JCClientCreate() {
    return new JCClient();
}

extern "C" DLL_EXPORT void JCClientDestroy(JCClientInterface *client) {
    delete client;
}
