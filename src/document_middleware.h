#pragma once
#include <fstream>
#include <boost/algorithm/string/trim.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "crow/http_request.h"
#include "crow/http_response.h"

namespace crow
{
	using namespace boost::filesystem;

	struct DocumentMiddleWare
	{
		std::string document_root;

		struct context
		{
		};

		DocumentMiddleWare() : document_root{ "." }
		{
		}

		void setDocumentRoot(const std::string& doc)
		{
			document_root = doc;
		}

		void before_handle(request& req, response& res, context& ctx)
		{
		}

		void after_handle(request& req, response& res, context& ctx)
		{
			if (res.code != 404)
				return;

			// TODO: check if file outside of document_root
			path filename{ document_root };
			// ignore url params
			filename /= req.url;

			CROW_LOG_DEBUG << "filename: " << filename.string();

			file_status s = status(filename);
			if (is_directory(s))
			{
				if (req.raw_url.back() != '/')
				{
					CROW_LOG_DEBUG << "directory without tail /, redirect with 301";
					res.code = 301;
					res.body.clear();
					res.set_header("Location", req.raw_url + '/');
					return;
				}

				filename /= "index.html";
				s = status(filename);
			}

			if (!is_regular_file(s))
			{
				CROW_LOG_DEBUG << "not a regular file";
				return;
			}

			const std::string spath{ filename.string() };
			std::ifstream inf(spath, std::ios::binary);
			if (!inf)
			{
				CROW_LOG_DEBUG << "failed to read file";
				res.code = 400;
				return;
			}

			std::string body{ std::istreambuf_iterator<char>(inf), std::istreambuf_iterator<char>() };
			res.code = 200;
			res.body = body;
			if (filename.has_extension())
			{
				std::string ext = filename.extension().string();
				boost::algorithm::to_lower(ext);
				static std::unordered_map<std::string, std::string> mimeTypes = {
					{ ".html", "text/html; charset=utf-8" },
					{ ".htm", "text/html; charset=utf-8" },
					{ ".js", "application/javascript; charset=utf-8" },
					{ ".css", "text/css; charset=utf-8" },
					{ ".gif", "image/gif" },
					{ ".png", "image/png" },
					{ ".webm", "video/webm" },
					{ ".mp4", "video/mp4" },
					{ ".jpg", "image/jpeg" },
				};

				if (ext == ".webm") {
					res.set_header("cache-control", "public, max-age=30672000");
				}

				const auto& mime = mimeTypes.find(ext);
				if (mime != mimeTypes.end())
				{
					res.set_header("Content-Type", mime->second);
				}
			}
		}
	};
}
