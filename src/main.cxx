#if WIN32
#include <windows.h>

void usleep(__int64 usec) 
{ 
    HANDLE timer; 
    LARGE_INTEGER ft; 

    ft.QuadPart = -(10*usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL); 
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0); 
    WaitForSingleObject(timer, INFINITE); 
    CloseHandle(timer); 
}

#else
#include <unistd.h>
#endif 

#include <stdint.h>
#include <memory.h>

#include "appinterface.h"

int main(int argc, char *argv[]) {
    AppInterface *app = CreateApp();

    int retval = app->ParseArgs(argc, argv);
    if (retval != -1) {
        return retval;
    }

    app->StartThread();

#if 0
    int width = 1280;
    int height = 720;
    int y_size = width*height;
    int u_size = width*height/4;
    int v_size = width*height/4;
    uint8_t *y = new uint8_t[y_size];
    uint8_t *u = new uint8_t[u_size];
    uint8_t *v = new uint8_t[v_size];       
    memset(y, 0, y_size);

    int count = 0;
    while (app->IsRunning()) {
        memset(y, count&0xff, y_size); 
        app->IncomingFrame(y, y_size, u, u_size, v, v_size, width, height, 0);
        usleep(33*1000);
        count++;
    }

    delete[] y;
    delete[] u;
    delete[] v;
#endif

    app->WaitForExit();
    
    return retval;
}
