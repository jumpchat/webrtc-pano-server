/****************************************************************************
 * Copyright (C) 2016, JumpChat                                             *
 * All rights reserved                                                      *
 *                                                                          *
 * For the licensing terms see LICENSE file inside the root directory.      *
 * For the list of contributors see AUTHORS file inside the root directory. *
 ****************************************************************************/
#pragma once

#include <string>
#include <map>
#include "jumpchat.h"

class JCClientInterface {
public:
    JCClientInterface() {}
    virtual ~JCClientInterface() {}

    /** @brief Connect to the jumpchat url.  If empty, it will default to the JumpChat main site
     */
    virtual void Connect(const std::map<std::string, std::string> &options) = 0;

    /** @brief Disconnect from the server
     */
    virtual void Disconnect() = 0;

    /** @brief Join jumpchat room
    */
    virtual bool JoinRoom(const std::string &url) = 0;
};


extern "C" {
	DLL_EXPORT JCClientInterface *JCClientCreate();
	DLL_EXPORT void JCClientDestroy(JCClientInterface *client);
}
