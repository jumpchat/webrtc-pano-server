/****************************************************************************
 * Copyright (C) 2016, FXPAL/JumpChat                                       *
 * All rights reserved                                                      *
 *                                                                          *
 * For the licensing terms see LICENSE file inside the root directory.      *
 * For the list of contributors see AUTHORS file inside the root directory. *
 ****************************************************************************/

#include "configfile.h"

#include "webrtc/base/logging.h"

ConfigFile::ConfigFile() {

}

ConfigFile::~ConfigFile() {

}

void ConfigFile::Load(const std::string &filename) {
    boost::property_tree::ini_parser::read_ini(filename, _config);
}

void ConfigFile::Save(const std::string &filename) {
    boost::property_tree::ini_parser::write_ini(filename, _config);
}


void ConfigFile::Set(const std::string &section, const std::string &key, const std::string &value) {
	_config.put(section + "." + key, value);
}

int ConfigFile::GetInt(const std::string &section, const std::string &key, int defaultValue) {
	std::string path = section + "." + key;
	return _config.get<int>(path, defaultValue);
}

bool ConfigFile::GetBool(const std::string &section, const std::string &key, bool defaultValue) {
	std::string path = section + "." + key;
	return _config.get<bool>(path, defaultValue);
}

std::string ConfigFile::GetString(const std::string &section, const std::string &key, const std::string &defaultValue) {
	std::string path = section + "." + key;
	return _config.get<std::string>(path, defaultValue);
}

crow::LogLevel ConfigFile::GetServerLogLevel() {
	std::string path = "server.loglevel";
	std::string level = _config.get<std::string>(path, "critical");

	if (level == "debug") {
		return crow::LogLevel::Debug;
	}
	else if (level == "info") {
		return crow::LogLevel::Info;
	}
	else if (level == "warning") {
		return crow::LogLevel::Warning;
	}
	else if (level == "error") {
		return crow::LogLevel::Error;
	}
	else if (level == "critical") {
		return crow::LogLevel::Critical;
	}
	return crow::LogLevel::Info;
}

rtc::LoggingSeverity ConfigFile::GetWebRTCLogLevel() {
	std::string path = "webrtc.loglevel";
	std::string level = _config.get<std::string>(path, "none");

	if (level == "sensitive") {
		return rtc::LS_SENSITIVE;
	}
	else if (level == "verbose") {
		return rtc::LS_VERBOSE;
	}
	else if (level == "info") {
		return rtc::LS_INFO;
	}
	else if (level == "warning") {
		return rtc::LS_WARNING;
	}
	else if (level == "error") {
		return rtc::LS_ERROR;
	}
	else if (level == "none") {
		return rtc::LS_NONE;
	}
	return rtc::LS_NONE;
}

