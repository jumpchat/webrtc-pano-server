set(PROJECT_BIN ${PROJECT_NAME})
set(PROJECT_LIB ${PROJECT_NAME}-app)
#set(PROJECT_LIB_STATIC ${PROJECT_NAME}-app-static)

## Creating Binaries for each *.cxx file
add_definitions(-DBOOST_NETWORK_ENABLE_HTTPS)
if (MSVC)
	add_definitions(-DWEBRTC_WIN=1 -D_CRT_SECURE_NO_WARNINGS -DNOMINMAX -D_WIN32_WINDOWS=0x06010000)
elseif (APPLE)
    add_definitions(-DWEBRTC_POSIX=1 -fvisibility=hidden -Wall -Werror -Wextra -Wpartial-availability -Wno-missing-field-initializers -Wno-unused-parameter -Wno-c++11-narrowing -Wno-covered-switch-default -Wno-deprecated-register -Wno-unneeded-internal-declaration -Wno-inconsistent-missing-override -Wno-shift-negative-value -Wno-extra-semi -Wno-unused-parameter
    )
    find_library(FOUNDATION_FRAMEWORK Foundation)
    find_library(COREAUDIO_FRAMEWORK CoreAudio)
    find_library(COREMEDIA_FRAMEWORK CoreMedia)
    find_library(COREVIDEO_FRAMEWORK CoreVideo)
    find_library(COREGRAPHICS_FRAMEWORK CoreGraphics)
    find_library(AUDIOTOOLBOX_FRAMEWORK AudioToolbox)
    find_library(AVFOUNDATION_FRAMEWORK AVFoundation)
    set(EXTRA_LIBS
        ${FOUNDATION_FRAMEWORK} ${COREAUDIO_FRAMEWORK} ${COREMEDIA_FRAMEWORK} 
        ${COREVIDEO_FRAMEWORK} ${AUDIOTOOLBOX_FRAMEWORK} ${COREGRAPHICS_FRAMEWORK}
        ${AVFOUNDATION_FRAMEWORK})
else()
    set(CMAKE_CXX_FLAGS_DEBUG "-D_GLIBCXX_DEBUG=1 -g")
    add_definitions(-DWEBRTC_POSIX=1 -D_GLIBCXX_USE_CXX11_ABI=0 -fvisibility=hidden -fvisibility-inlines-hidden -pthread -Wno-missing-field-initializers -Wno-unused-parameter -Wno-c++11-narrowing -Wno-covered-switch-default -Wno-deprecated-register -Wno-unneeded-internal-declaration -Wno-inconsistent-missing-override -Wno-shift-negative-value -Wno-undefined-var-template -Wno-nonportable-include-path -Wno-address-of-packed-member)
    set(CMAKE_EXE_LINKER_FLAGS "-Wall -std=gnu++11 -Wextra -Wl,--as-needed -Wl,--start-group")
    set(CMAKE_SHARED_LINKER_FLAGS "-Wall -std=gnu++11 -Wextra -Wl,--as-needed -Wl,--start-group") 
    set(EXTRA_LIBS -lX11 -ldl -levent)
endif()

file(GLOB_RECURSE PROJECT_SRCS ${MAINFOLDER}/src/*.cpp ${MAINFOLDER}/src/*.h)

#add_library(${PROJECT_LIB_STATIC} STATIC ${PROJECT_SRCS})
add_library(${PROJECT_LIB} SHARED ${PROJECT_SRCS})

if (WIN32)
    install(TARGETS ${PROJECT_LIB} ARCHIVE DESTINATION lib COMPONENT Applications)
    install(TARGETS ${PROJECT_LIB} RUNTIME DESTINATION bin COMPONENT Applications)
elseif(APPLE)
    install(TARGETS ${PROJECT_LIB}
        DESTINATION bin
        COMPONENT Applications)    
else()
    set(CMAKE_EXE_LINKER_FLAGS "-Wall -std=gnu++11 -Wextra -Wl,--as-needed -Wl,--start-group -Wl,-rpath,\$ORIGIN") 
    #add_custom_command(TARGET ${PROJECT_LIB} POST_BUILD
    #    COMMAND ${CMAKE_COMMAND} -E rename $<TARGET_FILE:${PROJECT_LIB}> $<TARGET_FILE:${PROJECT_LIB}>-obj.a
    #    COMMAND echo "create $<TARGET_FILE:${PROJECT_LIB}>" > $<TARGET_FILE:${PROJECT_LIB}>.mri
    #    COMMAND ls $<TARGET_FILE:${PROJECT_LIB}>-obj.a $<TARGET_FILE:webrtc> | xargs -I {} echo  addlib {} >> $<TARGET_FILE:${PROJECT_LIB}>.mri
    #    COMMAND echo "save" >> $<TARGET_FILE:${PROJECT_LIB}>.mri
    #    COMMAND echo "end" >> $<TARGET_FILE:${PROJECT_LIB}>.mri
    #    COMMAND ar -M < $<TARGET_FILE:${PROJECT_LIB}>.mri
    #)
    install(TARGETS ${PROJECT_LIB}
        DESTINATION bin
        COMPONENT Applications)    
endif()

target_link_libraries(${PROJECT_LIB} ${PROJECT_LIBS} ${EXTRA_LIBS})

add_executable(${PROJECT_BIN} ${MAINFOLDER}/src/main.cxx)
target_link_libraries(${PROJECT_BIN} ${PROJECT_LIB})

install(TARGETS ${PROJECT_BIN} DESTINATION bin COMPONENT Applications)

configure_file(${MAINFOLDER}/src/config.ini.in ${CMAKE_BINARY_DIR}/config.ini @ONLY)
install(FILES ${CMAKE_BINARY_DIR}/config.ini DESTINATION bin COMPONENT Config)

install(DIRECTORY ${MAINFOLDER}/static DESTINATION . COMPONENT Static)

install(FILES main.cxx DESTINATION src COMPONENT Sources)
install(FILES appinterface.h 
    jcclientinterface.h
    jumpchat.h
    DESTINATION include COMPONENT Sources)
install(FILES Makefile DESTINATION build COMPONENT Sources)


add_custom_target(runserver ${PROJECT_BIN} -c ${CMAKE_BINARY_DIR}/config.ini
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    DEPENDS ${PROJECT_BIN} COMMENT "Running server...")

#add_dependencies(${PROJECT_BIN} webrtc opencv)
