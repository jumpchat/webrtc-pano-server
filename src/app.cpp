#include "jumpchat.h"
#include "app.h"
#include "appinfo.h"
#include "ocvcapturer.h"
#include "jcclientinterface.h"

#include "webrtc/base/ssladapter.h"
#include "webrtc/base/thread.h"

#include "webrtc/base/checks.h"
#include "webrtc/api/peerconnectioninterface.h"
#include "webrtc/modules/video_capture/video_capture_factory.h"
#include "webrtc/media/engine/webrtcvideocapturerfactory.h"
#include "webrtc/modules/audio_device/include/audio_device.h"
#include "webrtc/modules/audio_device/audio_device_config.h"
#include "webrtc/modules/audio_device/audio_device_impl.h"
#include "webrtc/system_wrappers/include/critical_section_wrapper.h"

#include <cstdlib>
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

using namespace webrtc;
namespace po = boost::program_options;

#define URL_PREFIX "/api/v1/"

App::App() :
	_crit(CriticalSectionWrapper::CreateCriticalSection()),
	_custom_capturer(nullptr),
	_running(false)
{
	//_client = JCClientCreate();

	_app_thread = rtc::Thread::Create();
}

App::~App() { 
	//JCClientDestroy(_client);
	Stop();
	if (_custom_capturer) {
		delete _custom_capturer;
	}
	delete _crit;
}

std::string App::getProjectName() const {
    return APPLICATION_NAME;
}

std::string App::getProjectCodeName() const {
    return APPLICATION_CODENAME;
}

std::string App::getProjectVendorID() const {
    return APPLICATION_VENDOR_ID;
}

std::string App::getProjectVendorName() const {
    return APPLICATION_VENDOR_NAME;
}

std::string App::getProjectID() const {
    return APPLICATION_ID;
}

int App::getProjectMajorVersion() const {
    return APPLICATION_VERSION_MAJOR;
}

int App::getProjectMinorVersion() const {
    return APPLICATION_VERSION_MINOR;
}

int App::getProjectPatchVersion() const {
    return APPLICATION_VERSION_PATCH;
}

std::string App::getProjectVersion() const {
    return APPLICATION_VERSION_STRING;
}

std::string App::getProjectCopyrightYears() const {
    return APPLICATION_COPYRIGHT_YEARS;
}

webrtc::PeerConnectionFactoryInterface *App::factory() const {
	return _factory.get();
}

const std::map<std::string, PCObserver *> &App::observers() const {
	return _observers;
}

void App::addObserver(PCObserver *observer) {
	_observers[observer->id()] = observer;

}
	
cricket::VideoCapturer* App::openVideoCaptureDevice() {
	std::string cameraName = _config.GetString("webrtc", "camera", "THETA UVC HD Blender");
	CROW_LOG_INFO << "Looking for camera: " << cameraName;
	if (cameraName == "custom") {
		CROW_LOG_INFO << "Creating custom camera";
		_custom_capturer = new OCVCapturer();
		return _custom_capturer;
	}

	std::vector<std::string> device_names;
	GetVideoSources(device_names);

	// try to find RICOH
	cricket::WebRtcVideoDeviceCapturerFactory factory;
	cricket::VideoCapturer* capturer = nullptr;

	CROW_LOG_INFO << "Available cameras: ";
	CROW_LOG_INFO << "--------------------------------------";
	for (const auto& name : device_names) {
		CROW_LOG_INFO << " * " << name;
	}

	for (const auto& name : device_names) {
		if (name == cameraName) {
			CROW_LOG_INFO << "Found camera: " << cameraName;
			capturer = factory.Create(cricket::Device(name, 0));
			if (capturer) {
				break;
			}
		}
	}

	if (capturer == nullptr) {
		for (const auto& name : device_names) {
			capturer = factory.Create(cricket::Device(name, 0));
			if (capturer) {
				CROW_LOG_INFO << "Using camera: " << name;
				break;
			}
		}
	}

	if (capturer == nullptr) {
		CROW_LOG_INFO << "No camera avialable";
	}

	return capturer;
}

void App::Run(rtc::Thread *thread) {
	_running = true;
	
	// initialize
	Init();

	App &app = *this;

	std::map<PeerConnectionInterface *, PCObserver *> _observers;
	rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> _factory;

	std::string staticRoot = _config.GetString("server", "staticroot", "static");
	if (boost::filesystem::exists(staticRoot)) {
		staticRoot = boost::filesystem::canonical(staticRoot).string();
	}
	CROW_LOG_INFO << "staticRoot = " << staticRoot;
	app.get_middleware<crow::DocumentMiddleWare>().setDocumentRoot(staticRoot);

	// API routes
	CROW_ROUTE(app, URL_PREFIX "peerconnection")
		.methods("GET"_method, "POST"_method)
		([this](const crow::request& req, crow::response& resp) {
		crow::json::wvalue retval;
		SetHeaders(resp);

		if (req.method == "GET"_method) {
			retval = OnListPeerConnections(req);
		}
		else if (req.method == "POST"_method) {
			retval = OnCreatePeerConnection(req);
		}
		resp.write(crow::json::dump(retval));
		resp.end();
	});

	CROW_ROUTE(app, URL_PREFIX "peerconnection/<string>")
		.methods("GET"_method, "PUT"_method, "DELETE"_method, "OPTIONS"_method)
		([this](const crow::request& req, crow::response& resp, const std::string &id) {
		crow::json::wvalue retval;
		SetHeaders(resp);

		if (req.method == "GET"_method) {
			retval = OnGetPeerConnection(req, id);
			resp.write(crow::json::dump(retval));
			resp.end();
		}
		else if (req.method == "PUT"_method) {
			retval = OnUpdatePeerConnection(req, resp, id);
		}
		else if (req.method == "DELETE"_method) {
			retval = OnDeletePeerConnection(req, id);
			resp.write(crow::json::dump(retval));
			resp.end();
		}
		else if (req.method == "OPTIONS"_method) {
			resp.set_header("Allow", "GET, PUT, OPTIONS, DELETE");
			resp.set_header("Access-Control-Allow-Methods", "GET, PUT, OPTIONS, DELETE");
			resp.write(crow::json::dump(retval));
			resp.end();
		}
	});

	int port = _config.GetInt("server", "port", 8080);
    app.port(port).run();   

	_running = false;
	if (thread) {
		thread->Quit();
	}
}

int App::ParseArgs(int argc, char *argv[]) {

	po::options_description desc("Options");
    desc.add_options()
        ("help,h",      "Display this message")
		("config,c", po::value<std::string>(),   "Config file to use")
		("list,l", "List video and audio sources")
        ("version,v",   "Display the version number");

    po::variables_map varmap;

    try {
        store(po::command_line_parser(argc, argv).options(desc).run(), varmap);
        notify(varmap);
    } catch (po::error &error) {
        std::cerr << error.what() << std::endl;
        return EXIT_FAILURE;
    }

    if (varmap.count("help")) {
        std::cout << desc << std::endl;
		return EXIT_SUCCESS;
    }

    if (varmap.count("version")) {
        std::cout << getProjectVersion() << std::endl;
		return EXIT_SUCCESS;
	}

	if (varmap.count("list")) {
		std::vector<std::string> video_sources;
		std::vector<std::string> audio_sources;

		GetVideoSources(video_sources);
		GetAudioSources(audio_sources);

		std::cout << "Video Sources" << std::endl;
		std::cout << "-------------------------------------" << std::endl;
		for (const auto& name : video_sources) {
			std::cout << " * " << name << std::endl;
		}

		std::cout << std::endl << "Audio Sources" << std::endl;
		std::cout << "-------------------------------------" << std::endl;
		for (const auto& name : audio_sources) {
			std::cout << " * " << name << std::endl;
		}

		return EXIT_SUCCESS;
	}


	if (varmap.count("config")) {
		std::string filename = varmap["config"].as<std::string>();
		LoadConfig(filename);
	}
	else {
		boost::filesystem::path path = boost::filesystem::system_complete(argv[0]);
		path = path.parent_path();
		boost::filesystem::current_path(path);
		path.append("config.ini");
		LoadConfig(path.string());
	}

	return -1;
}

void App::Init() {
	webrtc::PeerConnectionInterface::IceServer iceServer;
	webrtc::PeerConnectionInterface::RTCConfiguration config;
	CROW_LOG_INFO << "Version: " << getProjectVersion();

	// connect to jumpchat
	//std::map<std::string, std::string> options;
	//options["serverURL"] = "https://staging.jump.chat";
	//_client->Connect(options);

	// set log level
	rtc::LogMessage::LogToDebug(_config.GetWebRTCLogLevel());
	crow::logger::setLogLevel(_config.GetServerLogLevel());

	rtc::InitializeSSL();
	rtc::InitRandom(rtc::Time());

	rtc::ThreadManager::Instance()->WrapCurrentThread();

	_network_thread =
		rtc::Thread::CreateWithSocketServer();
	_network_thread->SetName("network_thread", nullptr);
	RTC_CHECK(_network_thread->Start()) << "Failed to start thread";

	_worker_thread = rtc::Thread::Create();
	_worker_thread->SetName("worker_thread", nullptr);
	RTC_CHECK(_worker_thread->Start()) << "Failed to start thread";

	_signaling_thread = rtc::Thread::Create();
	_signaling_thread->SetName("signaling_thread", nullptr);
	RTC_CHECK(_signaling_thread->Start()) << "Failed to start thread";

	cricket::WebRtcVideoEncoderFactory *encoder_factory = nullptr;
	cricket::WebRtcVideoDecoderFactory *decoder_factory = nullptr;

	_factory = webrtc::CreatePeerConnectionFactory(
		_network_thread.get(), _worker_thread.get(), _signaling_thread.get(),
		nullptr, encoder_factory, decoder_factory);
	RTC_CHECK(_factory) << "Failed to create the peer connection factory; "
		<< "WebRTC/libjingle init likely failed on this device";

	_local_media = _factory->CreateLocalMediaStream("fxpal-pano-server");

	auto capturer = openVideoCaptureDevice();
	if (capturer != nullptr) {
		auto video_track_source = _factory->CreateVideoSource(capturer);
		auto video_track = _factory->CreateVideoTrack("webrtc-server-v0", video_track_source);
		_local_media->AddTrack(video_track);
	}

	bool enable_audio = _config.GetBool("webrtc", "enableAudio", true);
	CROW_LOG_INFO << "Enable audio: " << enable_audio;
	if (enable_audio) {
		auto audio_track_source = _factory->CreateAudioSource(NULL);
		auto audio_track = _factory->CreateAudioTrack("webrtc-server-a0", audio_track_source);
		_local_media->AddTrack(audio_track);
	}
}

void App::StartThread() {
	_app_thread->Start(this);
}

bool App::IsRunning() {
	return _running;
}

void App::WaitForExit() {
	_app_thread->Run();
}


void App::LoadConfig(const std::string &filename) {
	if (!boost::filesystem::exists(filename)) {
		LOG(LS_ERROR) << "Unable to load file: " << filename;
		return;
	}

	CROW_LOG_INFO << "Using config file: " << boost::filesystem::canonical(filename);;
	_config.Load(filename);
}


void App::SetConfig(const std::string &section, const std::string &name, const std::string &value) {
	_config.Set(section, name, value);	
}

void App::IncomingFrame(const uint8_t *y, int y_size,
        const uint8_t *u, int u_size,
        const uint8_t *v, int v_size,
        int width, int height, int rotation) {
	if (!_custom_capturer) {
		return;
	}

	_custom_capturer->IncomingFrame(y, y_size, u, u_size, v, v_size, width, height, rotation);
}

std::string App::CreatePeerConnection(const std::string &pc_config_json) {
	crow::json::rvalue pc_config;
	if (!pc_config_json.empty()) {
		pc_config = crow::json::load(pc_config_json);
	}

	PeerConnectionInterface::RTCConfiguration config(PeerConnectionInterface::RTCConfigurationType::kSafe);
	PeerConnectionInterface::IceServer googleIceServer;

	googleIceServer.urls.push_back("stun:stun.l.google.com:19302");
	config.servers.push_back(googleIceServer);

	// parse config
	if (pc_config.has("iceServers")) {
		const crow::json::rvalue &ice_servers = pc_config["iceServers"];
		size_t i = 0;
		for (i = 0; i < ice_servers.size(); i++) {
			const crow::json::rvalue &server = ice_servers[i];
			if (server.has("url")) {
				PeerConnectionInterface::IceServer iceServer;
				std::string url = server["url"].s();
				iceServer.urls.push_back(url);
				if (server.has("username")) {
					iceServer.username = server["username"].s();
				}
				if (server.has("credential")) {
					iceServer.password = server["credential"].s();
				}
				config.servers.push_back(iceServer);
			}
		}
	}

	PCObserver *observer = new PCObserver(this, nullptr);
	rtc::scoped_refptr<PeerConnectionInterface> pc = _factory->CreatePeerConnection(
		config,
		nullptr,
		nullptr,
		observer);

	pc->AddStream(_local_media);

	observer->pc(pc.get());
	addObserver(observer);

	if (pc.get() == nullptr) {
		return "";
	} else {
		return observer->id();
	}
}

bool App::DeletePeerConnection(const std::string &id) {

	bool retval = false;
	std::map<std::string, PCObserver *>::iterator iter = _observers.find(id);
	if (iter != _observers.end()) {
		PCObserver *obs = (*iter).second;

		obs->pc()->Close();
		delete obs;

		_observers.erase(iter);
		retval = true;
	}
	return retval;
}

std::vector<std::string> App::ListPeerConnections() {

	std::vector<std::string> ids;

	std::map<std::string, PCObserver *>::iterator iter;
	for (iter = _observers.begin(); iter != _observers.end(); iter++) {
		ids.push_back((*iter).first);
	}

	return ids;
}

void App::AddIceCandidate(const std::string &id, const std::string &candidate_json) {
	crow::json::rvalue candidate;
	if (candidate_json.empty())
		return;

	std::map<std::string, PCObserver *>::iterator iter = _observers.find(id);
	if (iter == _observers.end()) {
		return;
	}

	PCObserver *pco = (*iter).second;
	PeerConnectionInterface *pc = pco->pc();
		
	candidate = crow::json::load(candidate_json);

	const std::string &sdp_mid = candidate["sdpMid"].s();
	const std::string &sdp_candidate = candidate["candidate"].s();
	int sdp_mlineindex = (int)candidate["sdpMLineIndex"].i();

	SdpParseError error;
	std::unique_ptr<webrtc::IceCandidateInterface> ice_candidate(
		webrtc::CreateIceCandidate(sdp_mid, sdp_mlineindex, sdp_candidate, &error));

	pc->AddIceCandidate(ice_candidate.get());
}

void App::SetRemoteDescription(const std::string &id, const std::string &sdp_json) {

	crow::json::rvalue sdp;
	if (sdp_json.empty())
		return;

	std::map<std::string, PCObserver *>::iterator iter = _observers.find(id);
	if (iter == _observers.end()) {
		return;
	}

	sdp = crow::json::load(sdp_json);

	PCObserver *pco = (*iter).second;
	webrtc::PeerConnectionInterface *pc = pco->pc();

	const std::string &type = sdp["type"].s();
	const std::string &sdp_str = sdp["sdp"].s();
	webrtc::SessionDescriptionInterface *sdi = webrtc::CreateSessionDescription(type, sdp_str, nullptr);
	pc->SetRemoteDescription(pco, sdi);

}

void App::On(const std::string &name, Callback callback) {

}

void App::OnSetSDPSuccess(PCObserver *pco) {
	CROW_LOG_INFO << "OnSetSDPSuccess()";

	PeerConnectionInterface *pc = pco->pc();

	if (pc->local_description() == nullptr) {
		pco->pc()->CreateAnswer(pco, nullptr);
	}
	else {
		crow::response *resp = pco->resp();
		crow::json::wvalue retval;

		const SessionDescriptionInterface *sdp = pc->local_description();
		std::string sdp_string;
		sdp->ToString(&sdp_string);
		retval["result"] = "success";
		retval["id"] = pco->id();
		retval["answer"]["type"] = sdp->type();
		retval["answer"]["sdp"] = sdp_string;
		resp->write(crow::json::dump(retval));
		resp->end();
		pco->resp(nullptr);
	}

}

void App::OnSetSDPFailure(PCObserver *pco, const std::string &error) {
	CROW_LOG_INFO << "OnSetSDPFailure(): " << error;

	crow::response *resp = pco->resp();
	if (resp) {
		crow::json::wvalue retval;
		retval["result"] = "error";
		retval["errorMessage"] = error;
		resp->write(crow::json::dump(retval));
		resp->end();
		pco->resp(nullptr);
	}

}


crow::json::wvalue App::OnCreatePeerConnection(const crow::request& req) {
	auto req_json = crow::json::load(req.body);


	crow::json::wvalue retval;

	PeerConnectionInterface::RTCConfiguration config(PeerConnectionInterface::RTCConfigurationType::kSafe);
	PeerConnectionInterface::IceServer googleIceServer;

	googleIceServer.urls.push_back("stun:stun.l.google.com:19302");

	config.servers.push_back(googleIceServer);

	PCObserver *observer = new PCObserver(this, nullptr);
	rtc::scoped_refptr<PeerConnectionInterface> pc = _factory->CreatePeerConnection(
		config,
		nullptr,
		nullptr,
		observer);

	pc->AddStream(_local_media);

	observer->pc(pc.get());
	retval["result"] = pc.get() ? "success" : "error";
	if (pc.get() == nullptr) {
		retval["errorMessage"] = "CreatePeerConnecttion() failed";
	}
	else {
		retval["id"] = observer->id();
	}

	addObserver(observer);

	return retval;
}

crow::json::wvalue App::OnListPeerConnections(const crow::request& req) {
	crow::json::wvalue retval;

	retval["peerConnections"] = crow::json::rvalue(crow::json::type::List);
	std::map<std::string, PCObserver *>::iterator iter;
	int count = 0;
	crow::json::wvalue &pcs = retval["peerconnections"];
	for (iter = _observers.begin(); iter != _observers.end(); iter++) {
		crow::json::wvalue &item = pcs[count];

		WritePeerConnectionData(item, (*iter).second);

		count++;
	}

	return retval;
}

crow::json::wvalue App::OnGetPeerConnection(const crow::request &req, const std::string &id) {
	crow::json::wvalue retval;

	retval["id"] = id;

	std::map<std::string, PCObserver *>::iterator iter = _observers.find(id);

	if (iter != _observers.end()) {
		PCObserver *pco = (*iter).second;
		WritePeerConnectionData(retval, pco);
	}
	return retval;
}

crow::json::wvalue App::OnUpdatePeerConnection(const crow::request &req, crow::response &resp, const std::string &id) {
	auto json_req = crow::json::load(req.body);
	crow::json::wvalue retval;

	std::map<std::string, PCObserver *>::iterator iter = _observers.find(id);
	if (iter == _observers.end()) {
		resp = crow::response(404);
		resp.end();
		return retval;
	}

	PCObserver *pco = (*iter).second;
	webrtc::PeerConnectionInterface *pc = pco->pc();

	if (json_req.has("offer")) {
		pco->resp(&resp);
		std::string type = json_req["offer"]["type"].s();
		std::string sdp = json_req["offer"]["sdp"].s();
		webrtc::SessionDescriptionInterface *offerSDP = webrtc::CreateSessionDescription(type, sdp, nullptr);
		pc->SetRemoteDescription((*iter).second, offerSDP);

	} 
	
	if (json_req.has("iceCandidate")) {
		std::string sdp_mid = json_req["iceCandidate"]["sdpMid"].s();
		std::string sdp_candidate = json_req["iceCandidate"]["candidate"].s();
		int sdp_mlineindex = (int)json_req["iceCandidate"]["sdpMLineIndex"].i();

		SdpParseError error;
		std::unique_ptr<webrtc::IceCandidateInterface> candidate(
			webrtc::CreateIceCandidate(sdp_mid, sdp_mlineindex, sdp_candidate, &error));

		pc->AddIceCandidate(candidate.get());

		WritePeerConnectionData(retval, pco);

		resp.write(crow::json::dump(retval));
		resp.end();
	}

	//retval["id"] = id;
	return retval;
}

crow::json::wvalue App::OnDeletePeerConnection(const crow::request &req, const std::string &id) {
	crow::json::wvalue retval;

	std::map<std::string, PCObserver *>::iterator iter = _observers.find(id);
	if (iter != _observers.end()) {
		PCObserver *obs = (*iter).second;

		obs->pc()->Close();
		delete obs;

		_observers.erase(iter);
	}
	return retval;

}

void App::WritePeerConnectionData(crow::json::wvalue &retval, PCObserver *pco) {
	CriticalSectionScoped cs(_crit);

	PeerConnectionInterface *pc = pco->pc();

	retval["id"] = pco->id();
	retval["signaling_state"] = pc->signaling_state();
	retval["ice_gathering_state"] = pc->ice_gathering_state();
	retval["ice_connection_state"] = pc->ice_connection_state();

	std::vector<const IceCandidateInterface *> &candidates = pco->ice_candidates();
	std::vector<const IceCandidateInterface *>::iterator iter;
	int index = 0;
	for (iter = candidates.begin(); iter != candidates.end(); iter++) {
		retval["iceCandidates"][index]["sdpMid"] = (*iter)->sdp_mid();
		retval["iceCandidates"][index]["sdpMlineIndex"] = (*iter)->sdp_mline_index();

		std::string candidate;
		(*iter)->ToString(&candidate);
		retval["iceCandidates"][index]["candidate"] = candidate;

		index++;
	}

	for (iter = candidates.begin(); iter != candidates.end(); iter++) {
		delete (*iter);
	}
	candidates.clear();
}

void App::SetHeaders(crow::response& resp) {
	resp.set_header("Content-Type", "application/json");
	resp.set_header("Access-Control-Allow-Origin", _config.GetString("server", "cors", "*"));
}

void App::GetVideoSources(std::vector<std::string> &device_names) {
	device_names.clear();
	std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> info(
		webrtc::VideoCaptureFactory::CreateDeviceInfo());
	int num_devices = info->NumberOfDevices();
	for (int i = 0; i < num_devices; ++i) {
		const uint32_t kSize = 256;
		char name[kSize] = { 0 };
		char id[kSize] = { 0 };
		if (info->GetDeviceName(i, name, kSize, id, kSize) != -1) {
			device_names.push_back(name);
		}
	}
}

void App::GetAudioSources(std::vector<std::string> &device_names) {
	device_names.clear();
	static const int32_t kId = 1111;
	rtc::scoped_refptr<AudioDeviceModule> audio_device = webrtc::AudioDeviceModule::Create(
		kId, webrtc::AudioDeviceModule::kPlatformDefaultAudio);
	audio_device->Init();

	int num_devices = audio_device->RecordingDevices();
	for (int i = 0; i < num_devices; ++i) {
		char name[kAdmMaxDeviceNameSize] = { 0 };
		char id[kAdmMaxGuidSize] = { 0 };
		if (audio_device->RecordingDeviceName(i, name, id) != -1) {
			device_names.push_back(name);
		}
	}
}


void App::Stop() {
	_network_thread->Stop();
	_worker_thread->Stop();
	_signaling_thread->Stop();

	_app_thread->Stop();
}

extern "C" DLL_EXPORT AppInterface *CreateApp() {
	return new App();
}

extern "C" DLL_EXPORT void DeleteApp(AppInterface *app) {
	delete app;
}
