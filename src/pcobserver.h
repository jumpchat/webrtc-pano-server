/****************************************************************************
* Copyright (C) 2016, FXPAL/JumpChat                                       *
* All rights reserved                                                      *
*                                                                          *
* For the licensing terms see LICENSE file inside the root directory.      *
* For the list of contributors see AUTHORS file inside the root directory. *
***************************************************************************/
#pragma once

#include "webrtc/api/peerconnectioninterface.h"
#include "crow.h"

using namespace webrtc;

class App;

class PCObserver : public PeerConnectionObserver, public SetSessionDescriptionObserver, public CreateSessionDescriptionObserver {

public:
	PCObserver(App *app, PeerConnectionInterface *pc);
	virtual ~PCObserver();

	const std::string id() const;
	void pc(PeerConnectionInterface *pc);
	PeerConnectionInterface *pc() const;
	App *app() const;
	void resp(crow::response *resp);
	crow::response *resp() const;

	std::vector<const IceCandidateInterface *> &ice_candidates() { return _ice_candidates; }

	// Triggered when the SignalingState changed.
	virtual void OnSignalingChange(PeerConnectionInterface::SignalingState new_state);

	// TODO(deadbeef): Once all subclasses override the scoped_refptr versions
	// of the below three methods, make them pure virtual and remove the raw
	// pointer version.

	// Triggered when media is received on a new stream from remote peer.
	virtual void OnAddStream(rtc::scoped_refptr<MediaStreamInterface> stream);
	// Deprecated; please use the version that uses a scoped_refptr.
	virtual void OnAddStream(MediaStreamInterface* stream);

	// Triggered when a remote peer close a stream.
	virtual void OnRemoveStream(rtc::scoped_refptr<MediaStreamInterface> stream);
	// Deprecated; please use the version that uses a scoped_refptr.
	virtual void OnRemoveStream(MediaStreamInterface* stream);

	// Triggered when a remote peer opens a data channel.
	virtual void OnDataChannel(
		rtc::scoped_refptr<DataChannelInterface> data_channel);
	// Deprecated; please use the version that uses a scoped_refptr.
	virtual void OnDataChannel(DataChannelInterface* data_channel);

	// Triggered when renegotiation is needed. For example, an ICE restart
	// has begun.
	virtual void OnRenegotiationNeeded();

	// Called any time the IceConnectionState changes.
	virtual void OnIceConnectionChange(
		PeerConnectionInterface::IceConnectionState new_state);

	// Called any time the IceGatheringState changes.
	virtual void OnIceGatheringChange(
		PeerConnectionInterface::IceGatheringState new_state);

	// A new ICE candidate has been gathered.
	virtual void OnIceCandidate(const IceCandidateInterface* candidate);

	// Ice candidates have been removed.
	// TODO(honghaiz): Make this a pure virtual method when all its subclasses
	// implement it.
	virtual void OnIceCandidatesRemoved(
		const std::vector<cricket::Candidate>& candidates);

	// Called when the ICE connection receiving status changes.
	virtual void OnIceConnectionReceivingChange(bool receiving);

	// Called when a track is added to streams.
	// TODO(zhihuang) Make this a pure virtual method when all its subclasses
	// implement it.
	virtual void OnAddTrack(
		rtc::scoped_refptr<RtpReceiverInterface> receiver,
		const std::vector<rtc::scoped_refptr<MediaStreamInterface>>& streams);

	// SetSessionDescriptionObserver
	virtual int AddRef() const { return 1; };
	virtual int Release() const { return 1; };
	virtual void OnSuccess();
	virtual void OnFailure(const std::string& error);

	// CreateSessionDescriptionObserver
	virtual void OnSuccess(SessionDescriptionInterface* desc);



private:
	App *_app;
	rtc::scoped_refptr<PeerConnectionInterface> _pc;
	std::string _id;
	crow::response *_resp;
	std::vector<const IceCandidateInterface *> _ice_candidates;

};