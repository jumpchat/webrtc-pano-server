/****************************************************************************
 * Copyright (C) 2016, FXPAL/JumpChat                                       *
 * All rights reserved                                                      *
 *                                                                          *
 * For the licensing terms see LICENSE file inside the root directory.      *
 * For the list of contributors see AUTHORS file inside the root directory. *
 ***************************************************************************/
#pragma once

#include <string>
#include <vector>
#include "jumpchat.h"

class AppInterface {
public:
    AppInterface() {}
    virtual ~AppInterface() {}

    /** @brief Returns project name
     *
     * Returns the name of the project. May contain spaces, upper/lower 
     * letters and numbers
     * @return project name */
    virtual std::string getProjectName() const = 0;
    /** @brief Returns project code name
     *
     * Returns the code name of the project. Should be all lowercase 
     * without any spaces in name
     * @return project code name */
    virtual std::string getProjectCodeName() const = 0;
    /** @brief Returns project vendor id
     *
     * Returns the vendor id of the project. Contains dot connected 
     * lowercase words, usually the reverse company url such as
     * com.example for example
     * @return project vendor id */
    virtual std::string getProjectVendorID() const = 0;
    /** @brief Returns project vendor name
     *
     * Returns the vendor name of the project. May contain spaces
     * upper/lower letters or numbers. Usually the company name is
     * taken here
     * @return project vendor name */
    virtual std::string getProjectVendorName() const = 0;
    /** @brief Returns project id
     *
     * Returns the project id. Contains dot connected lowercase words,
     * usually the company url such as example.com for example 
     * @return project id */
    virtual std::string getProjectID() const = 0;
    /** @brief Returns project major version 
     *
     * Returns the projects major version. This value is usually
     * incremented for every major release 
     * @return project major version */
    virtual int getProjectMajorVersion() const = 0;
    /** @brief Returns project minor version
     *
     * Returns the projects minor version. This value is usually
     * incremented for every minor release 
     * @return project minor version */
    virtual int getProjectMinorVersion() const = 0;
    /** @brief Returns project patch version
     *
     * Returns the projects patch version. This value is usually
     * incremented for every released patch version
     * @return project patch version */
    virtual int getProjectPatchVersion() const = 0;
    /** @brief Returns project version
     *
     * Returns the project version. Contains the project major,
     * minor and patch version as well as the application version
     * type such as SNAPSHOT for example
     * @return project version */
    virtual std::string getProjectVersion() const = 0;
    /** @brief Returns project copyright years
     *
     * Returns the project copyright years. Usually the year in
     * which the last release was published 
     * @return project copyright years */
    virtual std::string getProjectCopyrightYears() const = 0;

    /** @brief parse command args
     *
     * Parse main args
     */
    virtual int ParseArgs(int argc, char *argv[]) = 0;

	/** @brief Initialize
     */
    virtual void Init() = 0;

	/** @brief Start the server thread
     *
     * Start server thread
     */
    virtual void StartThread() = 0;

	/** @brief Is server thread running
     */
    virtual bool IsRunning() = 0;

	/** @brief Wait for server thread to exit
     */
    virtual void WaitForExit() = 0;

	/** @brief Stop the app thread
     */
    virtual void Stop() = 0;

	/** @brief Load a config file
	 */
	virtual void LoadConfig(const std::string &filename) = 0;

	/** @brief Load a config file
	 */
    virtual void SetConfig(const std::string &section, const std::string &name, const std::string &value) = 0;

	/** @brief Update image if using custom capturer
	 */
    virtual void IncomingFrame(const uint8_t *y, int y_size,
        const uint8_t *u, int u_size,
        const uint8_t *v, int v_size,
        int width, int height, int rotation) = 0;

    /** @brief Create peer connection.  Returns uuid string to reference peer connection
     */
	virtual std::string CreatePeerConnection(const std::string &pc_config_json) = 0;

    /** @brief Delete peer connection.
     */    
    virtual bool DeletePeerConnection(const std::string &id) = 0;

    /** @brief List peer connections
     */    
	virtual std::vector<std::string> ListPeerConnections() = 0;

    /** @brief Add Ice Candidate for peer connection
     */    
	virtual void AddIceCandidate(const std::string &id, const std::string &candidate) = 0;

    /** @brief Set remote description
     */
    virtual void SetRemoteDescription(const std::string &id, const std::string &sdp) = 0;

    /** @brief Register callback.  Callbacks are "icecandidate" and 
     */
    // events are "icecandidate", "sdp"
    typedef void (*Callback)(const std::string &);
    virtual void On(const std::string &name, Callback callback) = 0;
};

extern "C" {
	DLL_EXPORT AppInterface *CreateApp();
	DLL_EXPORT void DeleteApp(AppInterface *app);
}
