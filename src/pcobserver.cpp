/****************************************************************************
* Copyright (C) 2016, FXPAL/JumpChat                                       *
* All rights reserved                                                      *
*                                                                          *
* For the licensing terms see LICENSE file inside the root directory.      *
* For the list of contributors see AUTHORS file inside the root directory. *
***************************************************************************/

#include "pcobserver.h"
#include "app.h"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

PCObserver::PCObserver(App *app, PeerConnectionInterface *pc) :
	_app(app),
	_pc(pc)
{
	boost::uuids::uuid uuid = boost::uuids::random_generator()();
	_id = boost::lexical_cast<std::string>(uuid);
}

PCObserver::~PCObserver() {

}

const std::string PCObserver::id() const {
	return _id;
}

void PCObserver::pc(PeerConnectionInterface *pc) {
	if (pc != _pc) {
		_pc = pc;
	}
}

PeerConnectionInterface *PCObserver::pc() const {
	return _pc.get();
}


App *PCObserver::app() const {
	return _app;
}

void PCObserver::resp(crow::response *resp) {
	if (resp != _resp) {
		_resp = resp;
	}
}

crow::response *PCObserver::resp() const {
	return _resp;
}

// Triggered when the SignalingState changed.
void PCObserver::OnSignalingChange(PeerConnectionInterface::SignalingState new_state) {
	// REQUIRED
	LOG(LS_INFO) << "PeerConnectionInterface: SignalingState = " << new_state;
}

// Triggered when media is received on a new stream from remote peer.
void PCObserver::OnAddStream(rtc::scoped_refptr<MediaStreamInterface> stream) {

}

// Deprecated; please use the version that uses a scoped_refptr.
void PCObserver::OnAddStream(MediaStreamInterface* stream) {}

// Triggered when a remote peer close a stream.
void PCObserver::OnRemoveStream(rtc::scoped_refptr<MediaStreamInterface> stream) {}

// Deprecated; please use the version that uses a scoped_refptr.
void PCObserver::OnRemoveStream(MediaStreamInterface* stream) {}

// Triggered when a remote peer opens a data channel.
void PCObserver::OnDataChannel(
	rtc::scoped_refptr<DataChannelInterface> data_channel) {};
// Deprecated; please use the version that uses a scoped_refptr.
void PCObserver::OnDataChannel(DataChannelInterface* data_channel) {}

// Triggered when renegotiation is needed. For example, an ICE restart
// has begun.
void PCObserver::OnRenegotiationNeeded() {
	// REQUIRED
}

// Called any time the IceConnectionState changes.
void PCObserver::OnIceConnectionChange(PeerConnectionInterface::IceConnectionState new_state) {
	// REQUIRED
	LOG(LS_INFO) << "OnIceConnectionChange: IceConnectionState = " << new_state;
}

// Called any time the IceGatheringState changes.
void PCObserver::OnIceGatheringChange(PeerConnectionInterface::IceGatheringState new_state) {
	//REQUIRED
	LOG(LS_INFO) << "OnIceGatheringChange: IceConnectionState = " << new_state;
}

// A new ICE candidate has been gathered.
void PCObserver::OnIceCandidate(const IceCandidateInterface* candidate) {
	//REQUIRED
	if (candidate) {
		std::string ice_candidate;
		candidate->ToString(&ice_candidate);
		LOG(LS_INFO) << "OnIceCandidate = " << ice_candidate;

		SdpParseError error;
		std::string candidateString;
		candidate->ToString(&candidateString);
		IceCandidateInterface* candidateCopy =  CreateIceCandidate(candidate->sdp_mid(),
										  candidate->sdp_mline_index(),
										  candidateString,
                                          &error);
		
		if (!error.description.empty()) {
			LOG(LS_ERROR) << error.description;
		}
		if (candidateCopy) {
			_ice_candidates.push_back(candidateCopy);
		}
	}
}

// Ice candidates have been removed.
// TODO(honghaiz): Make this a pure method when all its subclasses
// implement it.
void PCObserver::OnIceCandidatesRemoved(const std::vector<cricket::Candidate>& candidates) {}

// Called when the ICE connection receiving status changes.
void PCObserver::OnIceConnectionReceivingChange(bool receiving) {}

// Called when a track is added to streams.
// TODO(zhihuang) Make this a pure method when all its subclasses
// implement it.
void PCObserver::OnAddTrack(
	rtc::scoped_refptr<RtpReceiverInterface> receiver,
	const std::vector<rtc::scoped_refptr<MediaStreamInterface>>& streams) {}


void PCObserver::OnSuccess() {
	_app->OnSetSDPSuccess(this);
}

void PCObserver::OnSuccess(SessionDescriptionInterface* desc) {
	_pc->SetLocalDescription(this, desc);
}

void PCObserver::OnFailure(const std::string &error) {
	_app->OnSetSDPFailure(this, error);
}