/****************************************************************************
* Copyright (C) 2016, FXPAL/JumpChat                                       *
* All rights reserved                                                      *
*                                                                          *
* For the licensing terms see LICENSE file inside the root directory.      *
* For the list of contributors see AUTHORS file inside the root directory. *
***************************************************************************/
#pragma once

#include "webrtc/media/base/videocapturer.h"
#include "webrtc/base/thread.h"
#include "webrtc/common_video/include/i420_buffer_pool.h"

namespace webrtc {
    class VideoFrame;
    class CriticalSectionWrapper;
    class VideoCaptureExternal;
    class VideoCaptureModule;
}

class OCVCapturer : public cricket::VideoCapturer, public rtc::VideoSinkInterface<webrtc::VideoFrame> {
public:
    OCVCapturer();
    ~OCVCapturer();

    virtual bool GetBestCaptureFormat(const cricket::VideoFormat& desired, cricket::VideoFormat* best_format);
    virtual cricket::CaptureState Start(const cricket::VideoFormat& capture_format);
    virtual void Stop();
    virtual bool IsRunning();
    virtual bool IsScreencast() const { return false; };
    virtual bool GetPreferredFourccs(std::vector<uint32_t>* fourccs);
    virtual void OnFrame(const webrtc::VideoFrame &frame);

    void IncomingFrame(const uint8_t *y, int y_size,
        const uint8_t *u, int u_size,
        const uint8_t *v, int v_size,        
        int width, int height, int rotation);

private:
    bool _capturing;
    webrtc::CriticalSectionWrapper* _crit;
    webrtc::I420BufferPool _bufferPool;
    webrtc::VideoCaptureExternal *_capture_input_interface;
    rtc::scoped_refptr<webrtc::VideoCaptureModule> _module;
};
