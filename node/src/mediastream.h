#pragma once

#include "nan.h"
//#include "v8.h" // IWYU pragma: keep

#include "webrtc/api/mediastreaminterface.h"

namespace node_webrtc {

class MediaStream : public Nan::ObjectWrap
{
  public:
    explicit MediaStream(webrtc::MediaStreamInterface *mediaStream);
    ~MediaStream();

    //
    // Nodejs wrapping.
    //
    static void Init(v8::Handle<v8::Object> exports);
    static Nan::Persistent<v8::Function> constructor;
    static NAN_METHOD(New);
    static NAN_GETTER(label);

  private:
    webrtc::MediaStreamInterface *mediaStream;
};

} // namespace node_webrtc
