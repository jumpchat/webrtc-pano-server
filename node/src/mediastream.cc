#include "mediastream.h"

#include <string>
#include <vector>

#include "common.h"

using node_webrtc::MediaStream;
using v8::Array;
using v8::External;
using v8::Function;
using v8::FunctionTemplate;
using v8::Handle;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::String;
using v8::Value;

Nan::Persistent<Function> MediaStream::constructor;

MediaStream::MediaStream(webrtc::MediaStreamInterface* mediaStream)
: mediaStream(mediaStream) {}

MediaStream::~MediaStream() {
  mediaStream = nullptr;
}

NAN_METHOD(MediaStream::New) {
    return Nan::ThrowTypeError("Use PeerConnection to create a local stream");
}

void MediaStream::Init(v8::Handle<v8::Object> exports) {
    Local<FunctionTemplate> tpl = Nan::New<FunctionTemplate> (New);
    tpl->SetClassName(Nan::New("MediaStream").ToLocalChecked());
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    Nan::SetAccessor(tpl->InstanceTemplate(), Nan::New("label").ToLocalChecked(), label);
    constructor.Reset(tpl->GetFunction());
    exports->Set(Nan::New("MediaStream").ToLocalChecked(), tpl->GetFunction());
}

NAN_GETTER(MediaStream::label) {
  Nan::HandleScope scope;

  MediaStream *self = Nan::ObjectWrap::Unwrap<MediaStream>(info.Holder());
  std::string label = self->mediaStream->label();

  info.GetReturnValue().Set(Nan::New(label).ToLocalChecked());
}
