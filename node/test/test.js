var assert = require('assert');
var _ = require('lodash');
var wrtc = require('rtcapp')

describe('WebRTC', function () {
    describe('#create PeerConnection', function () {
        it('app should not be null', function () {
            var rtc = new wrtc.PeerConnection({
                'iceServers': [
                    { 'urls': ['stun:stun.l.google.com:19302'] }
                ]
            })
            assert(rtc);
        });
    });
});

// describe('App', function () {
//     describe('#create', function () {
//         it('app should not be null', function () {
//             var rtc = require('../build/Release/rtcapp');
//             var app = new rtc.App();
//             assert(app);
//         });
//     });

//     describe('#createPeerConnection', function () {
//         it('peer should be valid', function () {
//             var rtc = require('../build/Release/rtcapp');
//             var app = new rtc.App();
//             app.init();
//             assert(app);

//             var id = app.createPeerConnection({
//                 "iceServers":[
//                     { "url": "turn:162.222.181.65:3478?transport=udp", "credential": "/qs5aqL9SW+Jej15Z1yIlzVxeSU=", "username": "1487886697:jumpchat"},
//                     { "url": "turn:162.222.181.65:3478?transport=tcp", "credential": "/qs5aqL9SW+Jej15Z1yIlzVxeSU=", "username": "1487886697:jumpchat"}
//                 ]
//             });
//             assert(id);

//             var result = app.deletePeerConnection(id);
//             assert(result);

//             result = app.deletePeerConnection(id);
//             assert(!result);
//         });
//     });    
// });

// describe('JCClient', function () {
//     this.timeout(70000);

//     describe("#_guid", function() {
//         it('guid should not be a string', function () {
//             var jc = require('../jcclient');
//             var guid = jc._private._guid();
//             console.log(guid);
//             assert(guid);
//             assert(typeof(guid) === 'string');
//         });        
//     });

//     describe("#_fetchUserId", function() {
//         it('userid should not be a string', function () {
//             var jc = require('../jcclient');
//             var userid1 = jc._private._fetchUserId();
//             var userid2 = jc._private._fetchUserId();
//             assert(userid1 === userid2);
//             assert(typeof(userid1) === 'string');
//         });        
//     });    

//     describe('#create', function () {
//         it('jcclient should not be null', function () {
//             var jc = require('../jcclient');
//             var client = new jc.JCClient();
//             assert(client);
//         });
//     });

//     describe('#joinroom', function () {
//         it('join room succeeded', function (done) {
//             var jc = require('../jcclient');
//             var client = new jc.JCClient({
//                 'userid': '139d4da95678e9c501e2f04f4e35ae07cb3542e9'
//             });
//             clinet
//             client.joinUrl('https://stage.jumpch.at/yulius', '', 
//                 function (users) {
//                     console.log(users);
//                     assert(_.size(users) > 0);
//                     done();
//                 },
//                 function() {
//                     assert(false);
//                     done();
//                 }
//             );
//         });
//     });
// });
