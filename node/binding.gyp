{
    "targets": [
        {
            "target_name": "rtcapp",
            "sources": [
                "src/binding.cc",
                "src/create-answer-observer.cc",
                "src/create-offer-observer.cc",
                "src/datachannel.cc",
                "src/peerconnection.cc",
                "src/mediastream.cc",
                "src/rtcstatsreport.cc",
                "src/rtcstatsresponse.cc",
                "src/set-local-description-observer.cc",
                "src/set-remote-description-observer.cc",
                "src/stats-observer.cc",
            ],
            "include_dirs": [
                "../src",
                "./src",
                "../build/webrtc-src/src",
                "<!(node -e \"require('nan')\")"
            ],
            "link_settings": {
                "library_dirs": ["<(module_root_dir)/../build/lib/"],
                "libraries": [
                    "<(module_root_dir)/../build/lib/libwebrtc-server-appd.a",
                ],
                "conditions": [
                    ['OS=="mac"', {
                        "libraries": [
                            "-framework Foundation",
                            "-framework CoreAudio",
                            "-framework CoreMedia",
                            "-framework CoreVideo",
                            "-framework CoreGraphics",
                            "-framework AudioToolbox",
                            "-framework AVFoundation",
                            "-lboost_program_options",
                            "-lboost_system",
                            "-lboost_thread-mt",
                            "-lboost_regex",
                            "-lboost_filesystem",
                            "-lboost_chrono",
                            "-lboost_date_time",
                            "-lboost_random",
                        ]
                    }]
                ],
                #"libraries": [
                #    "-lwebrtc-server-appd",
                #    "-Wl,-rpath,<(module_root_dir)/../build/lib/"
                #]
            },
            'xcode_settings': {
                'MACOSX_DEPLOYMENT_TARGET': '10.9',
            },
            'defines': [
                'TRACING=1',
            ],
            'conditions': [
                ['OS=="linux"', {
                    'defines': [
                        '_GLIBCXX_USE_CXX11_ABI=0',
                        'WEBRTC_LINUX',
                        'WEBRTC_POSIX=1',
                    ],
                }],
                ['OS=="mac"', {
                    'defines': [
                        'WEBRTC_MAC',
                        'WEBRTC_IOS',
                        'WEBRTC_POSIX=1',
                    ],
                }],
                ['OS=="win"', {
                    'defines': [
                        'WEBRTC_WIN',
                        'NOGDI',
                        'NOMINMAX',
                    ],
                }],
            ]
        }
    ]
}
