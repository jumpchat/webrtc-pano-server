var rtc = require('rtcapp')
var morgan = require('morgan')
var uuid = require('node-uuid')
var _ = require('lodash')
var bodyParser = require('body-parser')

// webrtc
//var RTCPeerConnection = rtc.PeerConnection
var RTCPeerConnection = require('./peerconnection')

// var rtcapp = new rtc.App()
// rtcapp.setConfig('webrtc', 'camera', 'custom')
// rtcapp.init()
// console.log(rtcapp.getProjectName())

var express = require('express')
var app = express()
var router = express.Router(); 

var state =  {
    pcs: {}
}

// serve static
console.log(__dirname + '/../static')
app.use(express.static(__dirname + '/../static'))
app.use(morgan('combined'))
app.use(bodyParser.json('*/*'));
//app.use(bodyParser.urlencoded({ extended: true }));

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
})

function _pcoToJSON(id, pco) {
    if (!pco || !id) {
        return {}
    }

    var retval = {
        'id': id,
        'signalingState': _.get(pco, 'pc.signalingState'),
        'iceGatheringState': _.get(pco, 'pc.iceGatheringState'),
        'iceConnectionState': _.get(pco, 'pc.iceConnectionState'),
        'iceCandidates': _.get(pco, 'iceCandidates', [])
    }

    // clear ice iceCandidates
    pco.iceCandidates = []

    return retval
}

router.route('/peerconnection')
    .get(function(req, res) {
        var pcs = []
        _.forEach(state.pcs, function(pco, id) {
            pcs.push(_pcoToJSON(id, pco))
        })

        res.json({ peerConnections: pcs })
    })
    .post(function(req, res) {
        var options = {
            'iceServers': [
                { 'urls': ['stun:stun.l.google.com:19302'] }
            ]            
        }

        var id = uuid.v4()
        var pc = new RTCPeerConnection(options)
        var pco = {
            'pc': pc,
            'iceCandidates': []
        }
        state.pcs[id] = pco

        pc.onicecandidate = function(e) {
            var candidate = e.candidate;
            if (candidate) {
                var sdp = {
                    'candidate': candidate.candidate,
                    'sdpMid': candidate.sdpMid,
                    'sdpMLineIndex': candidate.sdpMLineIndex
                }
                pco.iceCandidates.push(sdp)
            }
        }

        pc.onsignalingstatechange = function(state) {
            console.log('onsignalingstatechange = ' + state)
        }

        pc.onicegatheringstatechange = function(state) {
            console.log('onicegatheringstatechange = ' + state)
        }

        res.json({
            'id': id
        })
    })

router.route('/peerconnection/:id')
    .get(function(req, res) {
        //res.json({ message: 'pc! ' + req.params['id'] });   
        var id = req.params['id']
        var pco = state.pcs[id]
        if (pco) {
            res.json(_pcoToJSON(id, pco))
        } else {
            res.send(404)
        }
    })
    .put(function(req,res) {
        var id = req.params['id']
        var pco = state.pcs[id]
        var errorCb = function() {
            console.log('setRemoteDescription error');
            res.json({})
        }
        if (pco) {
            if (req.body.offer) {
                var sdp = req.body.offer
                pco.pc.setRemoteDescription(sdp, function() {
                    pco.pc.createAnswer(function(sdp) {
                        pco.pc.setLocalDescription(sdp, function() {
                            console.log('setLocalDescription success');
                        }, errorCb)
                        res.json({
                            'result': 'success',
                            'id': 'id',
                            'answer': sdp
                        })
                    }, errorCb);
                }, errorCb)
            }


            if (req.body.iceCandidate) {
                var candidate = req.body.iceCandidate
                pco.pc.addIceCandidate(candidate)
                res.json({})
            }
        } else {
            res.send(404);
        }
    })
    .delete(function(req,res) {
        var id = req.params['id']
        delete state.pcs[id]
        res.json({})
    })

// respond with "hello world" when a GET request is made to the homepage
app.use('/api/v1/', router)

app.listen(8080, function () {
    // Put a friendly message on the terminal
    console.log("Server running at http://127.0.0.1:8080/")
})