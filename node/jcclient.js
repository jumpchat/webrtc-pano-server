(function (exports, global) {
    var io = require('socket.io-client');
    var rest = require('node-rest-client');
    var rtcapp = require('rtcapp');
    var _ = require('lodash');
    var localStorage = {};
    var _opts;
    var _apiKey;
    var _users = {};
    var _callbacks = {};
    var _offer = {};
    var _peers = {};

    // uncomment to disable console 
    //console = {'log': function() {} };

    function JCClient(opts) {
        this.client = new rest.Client();
        this.rtcapp = new rtcapp.App();
        
        _opts = opts || {};
        _apiKey = _opts.apiKey || 'db763c63-8e61-43a1-9f8c-f6c9ec9d27f8';
    }

    JCClient.prototype.joinUrl = function(url, password, success, fail) {
        var self = this;
        this.client.get(url + '/settings', {}, function (data, response) {
                if (data['serverUrl'] != '')
                    serverUrl = data['serverUrl'] + '/v1';
                self.roomSettings = data;
                var room = self.roomSettings['room'];
                var type = self.roomSettings['type'];
                _join(self, room, type, password, success, fail);
        }, fail);
    }

    JCClient.prototype.join = function(room, type, password, success, fail) {
        console.log('joinRoom: ' + room);
    }

    JCClient.prototype.on = function(name, cb) {
        _callbacks[name] = cb;
    }

    // private functions
    function _guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                 .toString(16)
                 .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    function _fetchUserId() {
        if (_.has(_opts, 'userid'))
            return _.get(_opts, 'userid');

        var userid = localStorage['userid'];
        if (!userid) {
            userid = _guid();
            localStorage['userid'] = userid;
        }
        return userid;
    }

    function _trigger(name, data) {
        var cb = _callbacks[name];
        var e = { 'type': name }
        data = data.slice(0);
        data.unshift(e);
        if (typeof(cb) == "function")
            cb.apply(e, data);
    }

    function _startPeerConnection(self, userid) {
        var pc;
        if (_peers[userid]) {
            pc = _peers[userid];
        } else {
            pc = peers[userid] = self.rtcapp.createPeerConnection(userid);
        }
        //var dc = dcs[userid] = pc.createDataChannel('jumpchat');
        //_setChannelEvents(self, userid, dc);

        offer[userid] = true;
        _startOffer(self, pc);
    }    

    function _join(self, room, type, password, success, fail) {
        var socket = io.connect(serverUrl, {'force new connection': _opts.forceNew});
        self.socket = socket;
        self.room = room;
        self.roomType = type;
        var userid = _fetchUserId();
        self.userid = userid;

        socket.on('connect', function() {
            console.log('on_connect');
            socket.emit('join', { 'room': room, 'userid': userid, 'password':password, 'type': type, 'apiKey': _apiKey });
            inRoom = true;

            if (self.roomSettings.mode == 'mcu') {
                _startPeerConnection(self, 'mcu');
            }
        });

        socket.on('disconnect', function() {
            console.log('on_disconnect');
        });

        socket.on('users', function(data, sessid) {
            console.log('users: ' + JSON.stringify(data));
            _users = _.merge(_users, data);
            self.sessid = sessid;
            for (var i in data) {
                var u = data[i];
                _users[u.userid] = u;

                // if MCU mode, we create a connection to users listed
                if (self.roomSettings.mode == 'mcu') {
                    if (sessid != u.userid)
                        _startPeerConnection(self, u.userid);
                }
            }

            // start broadcasting
            //self.startBroadcasting();

            if (success)
                success(_users);
        });

        socket.on('joined', function(data) {
            console.log('joined: ' + JSON.stringify(data));
            var userid = data.userid;
            users[userid] = data;
            data.newUser = true;

            // start connection
            _startPeerConnection(self, userid);
        });

        socket.on('left', function(data) {
            console.log('left: ' + JSON.stringify(data));
            var userid = data.userid;

            if (peers[userid] == undefined)
                return;

            //if (self.remoteVideoRemovedCallback)
            //    self.remoteVideoRemovedCallback(userid);
            _trigger('remoteVideoRemoved', [userid]);
            _trigger('userLeft', [userid]);
            delete users[userid];

            var peer = peers[userid];
            if (peer)
                peer.close();
            delete peers[userid];

            delete dcs[userid];
            delete stream[userid];

            // clean up files
            var keys = Object.keys(sendFiles);
            for (var i in keys) {
                var k = keys[i];
                var fp = sendFiles[k];
                if (fp.userid == userid) {
                    delete sendFiles[k];
                    _trigger('fileCancel', [userid, k]);
                }
            }

            keys = Object.keys(files);
            for (var i in keys) {
                var k = keys[i];
                var fp = files[k];
                if (fp.userid == userid) {
                    delete files[k];
                    _trigger('fileCancel', [userid, k]);
                }
            }
        });

        socket.on('room', function(data) {
            _password = data.password;
            _trigger('room', [data]);
            console.log('room: ' + JSON.stringify(data));
        });

        socket.on('kicked', function(data) {
            // kicked out
            _trigger('kicked', [data]);
        });

        socket.on('locked', function(data) {
            // room is locked and gave wrong password
            _trigger('locked', [data]);
        });

        socket.on('knocking', function(data) {
            _trigger('knocking', [data]);
        });

        socket.on('knockAnswer', function(data) {
            _trigger('knockAnswer', [data]);
        });

        socket.on('systemMessage', function(data) {
            _trigger('systemMessage', [data]);
        });

        socket.on('webrtc', function(data) {
            console.log('webrtc: ' + JSON.stringify(data));
            var from = data['from'];

            var pc = peers[from];
            if (!pc)
                pc = peers[from] = self.createPeerConnection(from);

            if (data['candidate']) {
                if (candidates[from]) {
                    candidates[from].push(data['candidate']);
                } else {
                    var pc = peers[from];
                    pc.addIceCandidate(new RTCIceCandidate(data['candidate']),
                        function() { }, // success
                        function(e) { console.error(e); } // fail
                    );
                }

            } else if (data['sdp']) {
                offer[from] = false;
                pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
                    console.log('remote desc set');
                    if (pc.remoteDescription.type == 'offer') {
                        pc.createAnswer(function(desc) {
                            desc.sdp = _setBandwidth(desc.sdp);
                            desc.sdp = _maybePreferCodec(desc.sdp, 'video', 'send', 'H264/90000');
                            desc.sdp = _maybePreferCodec(desc.sdp, 'video', 'receive', 'H264/90000');
                            pc.setLocalDescription(desc, function() {
                                var msg = {'to': from, 'sdp': { 'type':pc.localDescription.type, 'sdp':pc.localDescription.sdp } };
                                console.log('answer: ' + JSON.stringify(msg));
                                socket.emit('webrtc', msg);
                            }, console.error);
                        }, console.error, _getOptions());
                    }

                    self.drainCandidates(from, pc);
                }, function(e) {
                    console.error(e);
                });
            }
        });        
    }

    exports.JCClient = JCClient;
    exports._private = {
        '_join': _join,
        '_fetchUserId': _fetchUserId,
        '_guid': _guid
    }
})(module.exports, module.parent.exports)
