#include <nan.h>
#include "rtcapp.h"

void Init(v8::Local<v8::Object> exports) {
    RTCApp::Init(exports);
}

NODE_MODULE(rtcapp, Init)
