#include <nan.h>
#include "rtcapp.h"
#include "appinterface.h"

static std::string ToJSON(v8::Local<v8::Value> value)
{
    if (value.IsEmpty()) {
        return std::string();
    }
 
    v8::Local<v8::Object> json = Nan::GetCurrentContext()->Global()->Get(Nan::New("JSON").ToLocalChecked())->ToObject();
    v8::Local<v8::Function> stringify = json->Get(Nan::New("stringify").ToLocalChecked()).As<v8::Function>();
 
    //v8::Local<v8::Value> result = Nan::MakeCallback(json, stringify, 1,  &value);
    v8::Local<v8::Value> result = Nan::Callback(stringify).Call(json, 1, &value);
    v8::String::Utf8Value const str(result);
 
    return std::string(*str, str.length());
}

static std::string ToString(v8::Local<v8::Value> value) {
    v8::String::Utf8Value const str(value);
    std::string retval(*str, str.length());
    return retval;
}

Nan::Persistent<v8::Function> RTCApp::constructor;

RTCApp::RTCApp() {
    _app = CreateApp();
}

RTCApp::~RTCApp() {
    DeleteApp(_app);
}

  
void RTCApp::Init(v8::Local<v8::Object> exports) {
    Nan::HandleScope scope;

    // Prepare constructor template
    v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
    tpl->SetClassName(Nan::New("App").ToLocalChecked());
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    SetPrototypeMethod(tpl, "getProjectName", getProjectName);
    SetPrototypeMethod(tpl, "init", Init);
    SetPrototypeMethod(tpl, "setConfig", SetConfig);
    SetPrototypeMethod(tpl, "createPeerConnection", CreatePeerConnection);
    SetPrototypeMethod(tpl, "deletePeerConnection", DeletePeerConnection);
    SetPrototypeMethod(tpl, "listPeerConnections", ListPeerConnections);

    constructor.Reset(tpl->GetFunction());
    exports->Set(Nan::New("App").ToLocalChecked(), tpl->GetFunction());
}

void RTCApp::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
  if (info.IsConstructCall()) {
    // Invoked as constructor: `new RTCApp(...)`
    RTCApp* obj = new RTCApp();
    obj->Wrap(info.This());
    info.GetReturnValue().Set(info.This());
  } else {
    // Invoked as plain function `RTCApp(...)`, turn into construct call.
    const int argc = 0;
    v8::Local<v8::Value> argv[argc] = { };
    v8::Local<v8::Function> cons = Nan::New<v8::Function>(constructor);
    info.GetReturnValue().Set(cons->NewInstance(Nan::GetCurrentContext(), argc, argv).ToLocalChecked());
  }
}

void RTCApp::getProjectName(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    RTCApp const *wrapper = ObjectWrap::Unwrap<RTCApp>(info.Holder());
    if (wrapper && wrapper->_app) {
        std::string result = wrapper->_app->getProjectName();
        info.GetReturnValue().Set(Nan::New(result).ToLocalChecked());
    }
}

void RTCApp::Init(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    RTCApp const *wrapper = ObjectWrap::Unwrap<RTCApp>(info.Holder());
    if (wrapper && wrapper->_app) {
        wrapper->_app->Init();
    }
}

void RTCApp::SetConfig(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    if (info.Length() < 3) {
        return;
    }

    const std::string section = ToString(info[0]);
    const std::string name = ToString(info[1]);
    const std::string value = ToString(info[2]);
    RTCApp const *wrapper = ObjectWrap::Unwrap<RTCApp>(info.Holder());
    if (wrapper && wrapper->_app) {
        wrapper->_app->SetConfig(section, name, value);
    }
}

void RTCApp::CreatePeerConnection(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    RTCApp const *wrapper = ObjectWrap::Unwrap<RTCApp>(info.Holder());
    if (wrapper && wrapper->_app) {
        if (info.Length() == 1) {
            std::string pc_config = ToJSON(info[0]);

            std::string result = wrapper->_app->CreatePeerConnection(pc_config);
            info.GetReturnValue().Set(Nan::New(result).ToLocalChecked());
        }
    }
}

void RTCApp::DeletePeerConnection(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    RTCApp const *wrapper = ObjectWrap::Unwrap<RTCApp>(info.Holder());
    if (wrapper && wrapper->_app) {
        if (info.Length() == 1) {
            v8::String::Utf8Value const str(info[0]);
            std::string id(*str, str.length());
            
            bool result = wrapper->_app->DeletePeerConnection(id);
            info.GetReturnValue().Set(Nan::New(result));
        }
    }
}

void RTCApp::ListPeerConnections(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    RTCApp const *wrapper = ObjectWrap::Unwrap<RTCApp>(info.Holder());
    if (wrapper && wrapper->_app) {
        v8::String::Utf8Value const str(info[0]);
        std::string id(*str, str.length());
        
        std::vector<std::string> result = wrapper->_app->ListPeerConnections();
        //info.GetReturnValue().Set(resut);
    }
}
