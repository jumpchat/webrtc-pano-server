#pragma once

#include "appinterface.h"

class RTCApp : public Nan::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);

 private:
  explicit RTCApp();
  ~RTCApp();

  static void New(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void getProjectName(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void Init(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void SetConfig(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void CreatePeerConnection(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void DeletePeerConnection(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void ListPeerConnections(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static Nan::Persistent<v8::Function> constructor;
  AppInterface *_app;
};

