#include <string.h>
#include "gtest/gtest.h"

#include "jcclient.h"

/* to use a test fixture, we derive from testing::Test */
class JCClientTest : public testing::Test {

protected:
    /* will be called before each test is run */
    virtual void SetUp() {
        client = JCClientCreate();
    }

    /* will be called after each test is run */
    virtual void TearDown() {
        JCClientDestroy(client);
    }

    JCClientInterface *client;
};

/* using test fixture with TEST_F instead of TEST */
TEST_F(JCClientTest, Join) {
    //bool result = client->JoinRoom("https://stage.jump.chat/yulius");
    //ASSERT_EQ(result, true);
}

