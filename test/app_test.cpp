#include <string.h>
#include "gtest/gtest.h"

#include "appinterface.h"
#include "appinfo.h"

/* to use a test fixture, we derive from testing::Test */
class AppTest : public testing::Test {

protected:
    AppTest() : app(NULL) {

    }

    ~AppTest() {
        //DeleteApp(app);
    }

    /* will be called before each test is run */
    virtual void SetUp() {
        if (app == NULL) {
            app = CreateApp();
            app->SetConfig("server", "loglevel", "error");
            app->SetConfig("webrtc", "loglevel", "none");
            app->SetConfig("webrtc", "camera", "custom");
            app->Init();
        }
    }

    /* will be called after each test is run */
    virtual void TearDown() {
        //app->Stop();
        //DeleteApp(app);
    }
    AppInterface *app;
};

/* using test fixture with TEST_F instead of TEST */
TEST_F(AppTest, GetProjectName) {
    ASSERT_STREQ(app->getProjectName().c_str(), APPLICATION_NAME);
}

TEST_F(AppTest, GetProjectDetails) {
    ASSERT_STREQ(app->getProjectCodeName().c_str(), APPLICATION_CODENAME);
    ASSERT_STREQ(app->getProjectVendorID().c_str(), APPLICATION_VENDOR_ID);
    ASSERT_STREQ(app->getProjectVendorName().c_str(), APPLICATION_VENDOR_NAME);
    ASSERT_STREQ(app->getProjectID().c_str(), APPLICATION_ID);
    ASSERT_EQ(app->getProjectMajorVersion(), APPLICATION_VERSION_MAJOR);
    ASSERT_EQ(app->getProjectMinorVersion(), APPLICATION_VERSION_MINOR);
    ASSERT_EQ(app->getProjectPatchVersion(), APPLICATION_VERSION_PATCH);
    ASSERT_STREQ(app->getProjectVersion().c_str(), APPLICATION_VERSION_STRING);
    ASSERT_STREQ(app->getProjectCopyrightYears().c_str(), APPLICATION_COPYRIGHT_YEARS);
}


TEST_F(AppTest, PeerConnection) {
    std::string pc_config = "{"
        "\"iceServers\":["
            "{\"url\": \"turn:162.222.181.65:3478?transport=udp\", \"credential\": \"/qs5aqL9SW+Jej15Z1yIlzVxeSU=\", \"username\": \"1487886697:jumpchat\"},"
            "{\"url\": \"turn:162.222.181.65:3478?transport=tcp\", \"credential\": \"/qs5aqL9SW+Jej15Z1yIlzVxeSU=\", \"username\": \"1487886697:jumpchat\"}"
        "]"
    "}";
    std::string id = app->CreatePeerConnection(pc_config).c_str();
    ASSERT_STRNE(id.c_str(), "");

    std::vector<std::string> ids = app->ListPeerConnections();
    ASSERT_EQ(ids.size(), 1);
    ASSERT_STREQ(ids[0].c_str(), id.c_str());

    bool retval;
    retval = app->DeletePeerConnection(id);
    ASSERT_EQ(retval, true);

    retval = app->DeletePeerConnection(id);
    ASSERT_EQ(retval, false);    
}
