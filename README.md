# WebRTC Panoramic Server

FXPAL/JumpChat collaboration to create webrtc panoramic server.

## Prerequisites
This project requires:

 * Windows
     * Install [Visual Studio 2015 Update 3](https://www.visualstudio.com/downloads/)
     * Install [Scoop](http://scoop.sh/) (Windows only)
     * ```$ scoop install git cmake```
 * Linux
     * Install Ubuntu 14.04 (16.04 will not work)
     * ```$ sudo apt-get install make```
     * ```$ make deps```
 * Mac
    * Install [Xcode](https://itunes.apple.com/us/app/xcode/id497799835)
    * ```$ xcode-select --install```
    * Install [Homebrew](http://brew.sh/)
    * ```$ brew install cmake```
    

## Download & setup depot_tools
```
#!bash
$ cd $HOME
$ git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
$ export PATH=$HOME/depot_tools:"$PATH"
```

Make sure you can run `gclient` from the command line
```
#!bash
$ gclient --version
gclient.py 0.7
```


## Check out code

```
#!bash
$ git clone --recursive git@bitbucket.org:jumpchat/webrtc-pano-server.git
```

## Building

Windows
```
#!bash
$ cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=Debug
$ msbuild build\webrtc-pano-server.sln
```

Linux/Mac
   
```
#!bash
$ cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=Debug
$ make -C build
```

## Running

Windows
```
#!bash
$ build\bin\Debug\webrtc-pano-server.exe
```

Linux/Mac
```
#!bash
$ make -C build runserver
```

## Troubleshooting

* The build uses [Hunter](https://github.com/ruslo/hunter) to download library dependencies.  
  Be patient while it builds libraries like Boost