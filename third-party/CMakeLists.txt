## Thirdparty dependencies

# Crow HTTP Server
include_directories("${MAINFOLDER}/third-party/crow/include")
list(APPEND CMAKE_MODULE_PATH "${MAINFOLDER}/third-party/crow/cmake")

if (UNIX OR APPLE)
    add_definitions(-fvisibility=hidden -fPIC  -Wno-unused-parameter -Wno-sign-compare)
endif()

# WebRTC
add_subdirectory(jumpchat-webrtc)
#add_subdirectory(socket.io-client-cpp EXCLUDE_FROM_ALL)
add_subdirectory(cpp-netlib EXCLUDE_FROM_ALL)

# # Build OpenCV
# if (${CMAKE_CL_64})
#     set(TARGET_CPU_SIZE 64)
#     set(TARGET_CPU x64)
# else() 
#     set(TARGET_CPU_SIZE 32)
#     set(TARGET_CPU x86)
# endif()

# include(ExternalProject)
# ExternalProject_Add(opencv
#     PREFIX ${MAINFOLDER}/third-party/fxpal-opencv
#     BINARY_DIR ${CMAKE_BINARY_DIR}/opencv-build
#     SOURCE_DIR ${MAINFOLDER}/third-party/fxpal-opencv
#     INSTALL_DIR ${CMAKE_BINARY_DIR}/opencv

#     DOWNLOAD_COMMAND ""
#     UPDATE_COMMAND ""

#     CMAKE_GENERATOR ${CMAKE_GENERATOR}

#     CMAKE_ARGS 
#         -DBUILD_DOCS:BOOL=OFF
#         -DBUILD_EXAMPLES:BOOL=OFF
#         -DBUILD_NEW_PYTHON_SUPPORT:BOOL=OFF
#         -DBUILD_PACKAGE:BOOL=OFF
#         -DBUILD_SHARED_LIBS:BOOL=OFF
#         -DBUILD_PERF_TESTS:BOOL=OFF
#         -DBUILD_TESTS:BOOL=OFF
#         -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
#         -DWITH_FFMPEG:BOOL=OFF
#         -DWITH_AVFOUNDATION:BOOL=ON
#         -DWITH_QUICKTIME:BOOL=OFF
#         -DWITH_QTKIT:BOOL=OFF
#         -DBUILD_WITH_STATIC_CRT:BOOL=OFF
#         -DWITH_JPEG:BOOL=OFF
#         -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}/opencv
# )
